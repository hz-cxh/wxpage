const BASE_URL="https://demo.chengdupb.cn/server/"	
// const BASE_URL="https://www.chengdupb.cn/server/"
// const BASE_URL="http://192.168.4.155:9010/server/"
// const BASE_URL="http://192.168.4.108:9010/server/"
//const BASE_URL="https://cdpre.tfsmy.com/server/"
// const BASE_URL="https://tfsmy.chengdu.gov.cn/server/"
const HEADER={ 'content-type': 'application/x-www-form-urlencoded' }
export const selfRequest=(options)=>{
    return new Promise((resolve,reject)=>{
        uni.request({
            url:BASE_URL+options.url,
            method:options.method||'GET',
            header: HEADER,
            data:options.data||{},
            dataType: 'json',
            success:(res)=>{
                // if(res.data.status!==0){
                //     return uni.showToast({
                //         title:"获取数据失败"
                //     })
                // }
                resolve(res.data)
            },
            fail:(err)=>{
                reject(err);
            }
        })
    })
}
export const uploadRequest=(options)=>{
    return new Promise((resolve,reject)=>{
        uni.uploadFile({
            url:BASE_URL+options.url,
            filePath: options.filePath,
            header: { 'content-type': 'multipart/form-data' },
            name: 'file',
            formData:options.data,
            success:(res)=>{
                resolve(res.data)
            },
            fail:(err)=>{
                reject(err);
            }
        })
    })
}
