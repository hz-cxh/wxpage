  // #ifdef MP-WEIXIN
  import uma from 'umtrack-wx';
  uma.init({
      appKey:'617b8f0de014255fcb621199',
      useOpenid:false,
      autoGetOpenid:false,
      debug:true
  });
  // #endif
import Vue from 'vue'
import App from './App'
import store from './store'
import common from './common/common.js'
import {selfRequest,uploadRequest} from "./common/request"
import cuCustom from './colorui/components/cu-custom.vue'
import wxShare from './utils/wxShare.js'

//在原型中挂载
Vue.prototype.$selfRequest=selfRequest;
Vue.prototype.$uploadRequest=uploadRequest;
Vue.component('cu-custom', cuCustom)

Vue.config.productionTip = false
Vue.prototype.$store = store
Vue.prototype.$noMultipleClicks = common.noMultipleClicks;
Vue.mixin(wxShare)
/**
 * 积分功能
 * @param {Object} params
 */
Vue.prototype.$memberOperLog = function(params){
	let identity = uni.getStorageSync("identity");

	if(identity == 1){
		uni.request({
			url:this.$store.state.websiteUrl + 'api/memberOperateLog/addMemberOperLog',
			method:"POST",
			header: {
				'content-type': 'application/x-www-form-urlencoded',
			},
			data: params,
			dataType:"json"
		})
		
	}
}
  // uma.init({
  //   appKey: "617b8f0de014255fcb621199",
  //   useOpenid: false,
  //   autoGetOpenid: false,
  //   debug: true,
  // });
  // uma.install = function (Vue) {
  //   Vue.prototype.$uma = uma;
  // };
  // Vue.use(uma);

  Vue.config.productionTip =false
  // #ifdef MP-WEIXIN
  Vue.use(uma);
  // #endif
/**
 * 咸鱼日志记录
 * @param {Object} params
 */
Vue.prototype.$memberOperateLog = function(params){
	let identity = uni.getStorageSync("identity");
	let longitude,latitude 
	if(identity == 1){
		uni.getLocation({
			
			success: (res) => {
				latitude=res.latitude
				longitude=res.longitude
				const data = {}
				Object.assign(data,{...params,latitude,longitude})
				uni.request({
					url:this.$store.state.websiteUrl + 'api/memberOperateLog/memberOperateLog',
					method:"POST",
					header: {
						'content-type': 'application/x-www-form-urlencoded',
					},
					data: data,
					dataType:"json",
				})
			}
		})
		
	}
}

Vue.prototype.eLogger = function(type, txt,pid) {
	if(pid == undefined|| pid == ''){
		pid = ''
	}else{
		pid = pid
	}
}
Vue.prototype.$addMemberShare = function(tache,addoptions,uid,pid){
				addoptions.tache=tache
				addoptions.userId = uid || ''
				addoptions.projectId = pid || ""
				
				uni.request({
					url:this.$store.state.websiteUrl+'api/addMemberShareRecord',
					header:{'content-type': 'application/x-www-form-urlencoded'},
					method:'POST',
					data:addoptions,
					success: (res) => {
						
					}
				})
			},
Vue.prototype.$isLogin = function(options){
	
				let pages = getCurrentPages()
				let page = pages[pages.length-1]
				let route = '/'+page.route
				
				if(!uni.getStorageSync('userinfo')){
					this.$addMemberShare(0,options)
					uni.showModal({
						title:'您还未登录，请登录后操作',
						showCancel:false,
						success: (res) => {
							if(res.confirm){
								uni.switchTab({
									url:'../about/about',
									success: (res) => {
										if(res.errMsg==="switchTab:ok"){
											uni.setStorage({
												key:'shareData',
												data:options
											})
											uni.setStorageSync('route',route)
										}
									}
								})
							}
						}
					})
				}else{
					// this.$addMemberShare(0,options)
					// console.log(this.$addMemberShare(options),'this');
					// console.log(Vue.prototype.$addMemberShare(),'Vue.prototype.$addMemberShare');
					if(uni.getStorageSync('cur_id')!=options.shareUserId){
						this.$addMemberShare(0,options)
					}
				}
				
			},
App.mpType = 'app'

const app = new Vue({
	store,
	...App
})
app.$mount()
