export default {
//发送给朋友
  data(){
	  return{
		  wxshare:{
			  shareUserId:'',
			  shareTime:"",
			  tache:'',
			  type:2,
			  projectId:'',
			  userId:''
		  }
	  }
  },
  onShareAppMessage(res) {
    this.wxshare.shareTime  = parseInt(Date.now()/1000)
    this.wxshare.shareUserId=uni.getStorageSync('cur_id')
	let pages = getCurrentPages()
	let page = pages[pages.length-1]
    return {
      title: '我分享了一个应用给你',
      path: `${page.route}?share=${encodeURIComponent(JSON.stringify(this.wxshare))}`
    }
  },
  //分享到朋友圈
  onShareTimeline(res) {
	  this.wxshare.shareTime  = parseInt(Date.now()/1000)
	  this.wxshare.shareUserId=uni.getStorageSync('cur_id')
	  let pages = getCurrentPages()
	  let page = pages[pages.length-1]
     return {
      title: '我分享了一个应用给你',
       path: `${page.route}?share=${encodeURIComponent(JSON.stringify(this.wxshare))}`
    }
  }
}