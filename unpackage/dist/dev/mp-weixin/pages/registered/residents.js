(global["webpackJsonp"] = global["webpackJsonp"] || []).push([["pages/registered/residents"],{

/***/ 61:
/*!**********************************************************************************************************!*\
  !*** C:/Users/13072/Documents/HBuilderProjects/wxpage/main.js?{"page":"pages%2Fregistered%2Fresidents"} ***!
  \**********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(wx, createPage) {

var _interopRequireDefault = __webpack_require__(/*! @babel/runtime/helpers/interopRequireDefault */ 4);
__webpack_require__(/*! uni-pages */ 26);
var _vue = _interopRequireDefault(__webpack_require__(/*! vue */ 25));
var _residents = _interopRequireDefault(__webpack_require__(/*! ./pages/registered/residents.vue */ 62));
// @ts-ignore
wx.__webpack_require_UNI_MP_PLUGIN__ = __webpack_require__;
createPage(_residents.default);
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./node_modules/@dcloudio/uni-mp-weixin/dist/wx.js */ 1)["default"], __webpack_require__(/*! ./node_modules/@dcloudio/uni-mp-weixin/dist/index.js */ 2)["createPage"]))

/***/ }),

/***/ 62:
/*!***************************************************************************************!*\
  !*** C:/Users/13072/Documents/HBuilderProjects/wxpage/pages/registered/residents.vue ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _residents_vue_vue_type_template_id_3200cecf___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./residents.vue?vue&type=template&id=3200cecf& */ 63);
/* harmony import */ var _residents_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./residents.vue?vue&type=script&lang=js& */ 65);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _residents_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _residents_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__[key]; }) }(__WEBPACK_IMPORT_KEY__));
/* harmony import */ var _residents_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./residents.vue?vue&type=style&index=0&lang=css& */ 67);
/* harmony import */ var _D_HBuilderX_3_8_12_20230817_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/@dcloudio/vue-cli-plugin-uni/packages/vue-loader/lib/runtime/componentNormalizer.js */ 33);

var renderjs





/* normalize component */

var component = Object(_D_HBuilderX_3_8_12_20230817_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _residents_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _residents_vue_vue_type_template_id_3200cecf___WEBPACK_IMPORTED_MODULE_0__["render"],
  _residents_vue_vue_type_template_id_3200cecf___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null,
  false,
  _residents_vue_vue_type_template_id_3200cecf___WEBPACK_IMPORTED_MODULE_0__["components"],
  renderjs
)

component.options.__file = "pages/registered/residents.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ 63:
/*!**********************************************************************************************************************!*\
  !*** C:/Users/13072/Documents/HBuilderProjects/wxpage/pages/registered/residents.vue?vue&type=template&id=3200cecf& ***!
  \**********************************************************************************************************************/
/*! exports provided: render, staticRenderFns, recyclableRender, components */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _D_HBuilderX_3_8_12_20230817_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_D_HBuilderX_3_8_12_20230817_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_preprocess_loader_index_js_ref_17_0_D_HBuilderX_3_8_12_20230817_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_webpack_uni_mp_loader_lib_template_js_D_HBuilderX_3_8_12_20230817_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_uni_app_loader_page_meta_js_D_HBuilderX_3_8_12_20230817_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_index_js_vue_loader_options_D_HBuilderX_3_8_12_20230817_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_webpack_uni_mp_loader_lib_style_js_residents_vue_vue_type_template_id_3200cecf___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!./node_modules/@dcloudio/vue-cli-plugin-uni/packages/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/@dcloudio/vue-cli-plugin-uni/packages/webpack-preprocess-loader??ref--17-0!./node_modules/@dcloudio/webpack-uni-mp-loader/lib/template.js!./node_modules/@dcloudio/vue-cli-plugin-uni/packages/webpack-uni-app-loader/page-meta.js!./node_modules/@dcloudio/vue-cli-plugin-uni/packages/vue-loader/lib??vue-loader-options!./node_modules/@dcloudio/webpack-uni-mp-loader/lib/style.js!./residents.vue?vue&type=template&id=3200cecf& */ 64);
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _D_HBuilderX_3_8_12_20230817_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_D_HBuilderX_3_8_12_20230817_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_preprocess_loader_index_js_ref_17_0_D_HBuilderX_3_8_12_20230817_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_webpack_uni_mp_loader_lib_template_js_D_HBuilderX_3_8_12_20230817_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_uni_app_loader_page_meta_js_D_HBuilderX_3_8_12_20230817_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_index_js_vue_loader_options_D_HBuilderX_3_8_12_20230817_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_webpack_uni_mp_loader_lib_style_js_residents_vue_vue_type_template_id_3200cecf___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _D_HBuilderX_3_8_12_20230817_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_D_HBuilderX_3_8_12_20230817_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_preprocess_loader_index_js_ref_17_0_D_HBuilderX_3_8_12_20230817_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_webpack_uni_mp_loader_lib_template_js_D_HBuilderX_3_8_12_20230817_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_uni_app_loader_page_meta_js_D_HBuilderX_3_8_12_20230817_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_index_js_vue_loader_options_D_HBuilderX_3_8_12_20230817_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_webpack_uni_mp_loader_lib_style_js_residents_vue_vue_type_template_id_3200cecf___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "recyclableRender", function() { return _D_HBuilderX_3_8_12_20230817_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_D_HBuilderX_3_8_12_20230817_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_preprocess_loader_index_js_ref_17_0_D_HBuilderX_3_8_12_20230817_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_webpack_uni_mp_loader_lib_template_js_D_HBuilderX_3_8_12_20230817_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_uni_app_loader_page_meta_js_D_HBuilderX_3_8_12_20230817_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_index_js_vue_loader_options_D_HBuilderX_3_8_12_20230817_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_webpack_uni_mp_loader_lib_style_js_residents_vue_vue_type_template_id_3200cecf___WEBPACK_IMPORTED_MODULE_0__["recyclableRender"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "components", function() { return _D_HBuilderX_3_8_12_20230817_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_D_HBuilderX_3_8_12_20230817_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_preprocess_loader_index_js_ref_17_0_D_HBuilderX_3_8_12_20230817_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_webpack_uni_mp_loader_lib_template_js_D_HBuilderX_3_8_12_20230817_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_uni_app_loader_page_meta_js_D_HBuilderX_3_8_12_20230817_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_index_js_vue_loader_options_D_HBuilderX_3_8_12_20230817_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_webpack_uni_mp_loader_lib_style_js_residents_vue_vue_type_template_id_3200cecf___WEBPACK_IMPORTED_MODULE_0__["components"]; });



/***/ }),

/***/ 64:
/*!**********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/@dcloudio/vue-cli-plugin-uni/packages/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/@dcloudio/vue-cli-plugin-uni/packages/webpack-preprocess-loader??ref--17-0!./node_modules/@dcloudio/webpack-uni-mp-loader/lib/template.js!./node_modules/@dcloudio/vue-cli-plugin-uni/packages/webpack-uni-app-loader/page-meta.js!./node_modules/@dcloudio/vue-cli-plugin-uni/packages/vue-loader/lib??vue-loader-options!./node_modules/@dcloudio/webpack-uni-mp-loader/lib/style.js!C:/Users/13072/Documents/HBuilderProjects/wxpage/pages/registered/residents.vue?vue&type=template&id=3200cecf& ***!
  \**********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns, recyclableRender, components */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "recyclableRender", function() { return recyclableRender; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "components", function() { return components; });
var components
try {
  components = {
    wPicker: function () {
      return __webpack_require__.e(/*! import() | components/w-picker/w-picker */ "components/w-picker/w-picker").then(__webpack_require__.bind(null, /*! @/components/w-picker/w-picker.vue */ 985))
    },
    hzModal: function () {
      return __webpack_require__.e(/*! import() | components/hz-modal/hz-modal */ "components/hz-modal/hz-modal").then(__webpack_require__.bind(null, /*! @/components/hz-modal/hz-modal.vue */ 992))
    },
  }
} catch (e) {
  if (
    e.message.indexOf("Cannot find module") !== -1 &&
    e.message.indexOf(".vue") !== -1
  ) {
    console.error(e.message)
    console.error("1. 排查组件名称拼写是否正确")
    console.error(
      "2. 排查组件是否符合 easycom 规范，文档：https://uniapp.dcloud.net.cn/collocation/pages?id=easycom"
    )
    console.error(
      "3. 若组件不符合 easycom 规范，需手动引入，并在 components 中注册该组件"
    )
  } else {
    throw e
  }
}
var render = function () {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
}
var recyclableRender = false
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ 65:
/*!****************************************************************************************************************!*\
  !*** C:/Users/13072/Documents/HBuilderProjects/wxpage/pages/registered/residents.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _D_HBuilderX_3_8_12_20230817_HBuilderX_plugins_uniapp_cli_node_modules_babel_loader_lib_index_js_D_HBuilderX_3_8_12_20230817_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_preprocess_loader_index_js_ref_13_1_D_HBuilderX_3_8_12_20230817_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_webpack_uni_mp_loader_lib_script_js_D_HBuilderX_3_8_12_20230817_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_index_js_vue_loader_options_D_HBuilderX_3_8_12_20230817_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_webpack_uni_mp_loader_lib_style_js_residents_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!./node_modules/babel-loader/lib!./node_modules/@dcloudio/vue-cli-plugin-uni/packages/webpack-preprocess-loader??ref--13-1!./node_modules/@dcloudio/webpack-uni-mp-loader/lib/script.js!./node_modules/@dcloudio/vue-cli-plugin-uni/packages/vue-loader/lib??vue-loader-options!./node_modules/@dcloudio/webpack-uni-mp-loader/lib/style.js!./residents.vue?vue&type=script&lang=js& */ 66);
/* harmony import */ var _D_HBuilderX_3_8_12_20230817_HBuilderX_plugins_uniapp_cli_node_modules_babel_loader_lib_index_js_D_HBuilderX_3_8_12_20230817_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_preprocess_loader_index_js_ref_13_1_D_HBuilderX_3_8_12_20230817_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_webpack_uni_mp_loader_lib_script_js_D_HBuilderX_3_8_12_20230817_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_index_js_vue_loader_options_D_HBuilderX_3_8_12_20230817_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_webpack_uni_mp_loader_lib_style_js_residents_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_D_HBuilderX_3_8_12_20230817_HBuilderX_plugins_uniapp_cli_node_modules_babel_loader_lib_index_js_D_HBuilderX_3_8_12_20230817_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_preprocess_loader_index_js_ref_13_1_D_HBuilderX_3_8_12_20230817_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_webpack_uni_mp_loader_lib_script_js_D_HBuilderX_3_8_12_20230817_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_index_js_vue_loader_options_D_HBuilderX_3_8_12_20230817_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_webpack_uni_mp_loader_lib_style_js_residents_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _D_HBuilderX_3_8_12_20230817_HBuilderX_plugins_uniapp_cli_node_modules_babel_loader_lib_index_js_D_HBuilderX_3_8_12_20230817_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_preprocess_loader_index_js_ref_13_1_D_HBuilderX_3_8_12_20230817_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_webpack_uni_mp_loader_lib_script_js_D_HBuilderX_3_8_12_20230817_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_index_js_vue_loader_options_D_HBuilderX_3_8_12_20230817_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_webpack_uni_mp_loader_lib_style_js_residents_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _D_HBuilderX_3_8_12_20230817_HBuilderX_plugins_uniapp_cli_node_modules_babel_loader_lib_index_js_D_HBuilderX_3_8_12_20230817_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_preprocess_loader_index_js_ref_13_1_D_HBuilderX_3_8_12_20230817_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_webpack_uni_mp_loader_lib_script_js_D_HBuilderX_3_8_12_20230817_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_index_js_vue_loader_options_D_HBuilderX_3_8_12_20230817_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_webpack_uni_mp_loader_lib_style_js_residents_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_D_HBuilderX_3_8_12_20230817_HBuilderX_plugins_uniapp_cli_node_modules_babel_loader_lib_index_js_D_HBuilderX_3_8_12_20230817_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_preprocess_loader_index_js_ref_13_1_D_HBuilderX_3_8_12_20230817_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_webpack_uni_mp_loader_lib_script_js_D_HBuilderX_3_8_12_20230817_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_index_js_vue_loader_options_D_HBuilderX_3_8_12_20230817_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_webpack_uni_mp_loader_lib_style_js_residents_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ 66:
/*!***********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib!./node_modules/@dcloudio/vue-cli-plugin-uni/packages/webpack-preprocess-loader??ref--13-1!./node_modules/@dcloudio/webpack-uni-mp-loader/lib/script.js!./node_modules/@dcloudio/vue-cli-plugin-uni/packages/vue-loader/lib??vue-loader-options!./node_modules/@dcloudio/webpack-uni-mp-loader/lib/style.js!C:/Users/13072/Documents/HBuilderProjects/wxpage/pages/registered/residents.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(uni, wx) {

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
var _default = {
  data: function data() {
    return {
      modal: false,
      content: '',
      linkageVisible: false,
      avatar: '',
      nickname: '',
      code_show: true,
      checkcode: '',
      openid: '',
      name: '',
      area: '',
      mobile: '',
      session_code: '',
      code_server: '',
      area_id: '',
      street_id: '',
      com_id: '',
      linkList: [],
      qr_com: 0,
      qr_are: 0,
      qr_str: 0,
      inputVal: 60,
      linkageVal: [64, 89, 73],
      ss: 'sobot|神秘人|https://wx.qlogo.cn/mmopen/vi_32/DYAIOgq83erU60ydaRhTkftHZBfB9rSQtarvgejZD7gktJ10W5aic8mUVjplsQOKIsms2vv2rlWrytXswpe2eXQ/132',
      scene: null,
      // scene 分享者带来的 参数,
      mid: '',
      areaTag: true,
      basedata: null,
      aname: null,
      sname: null,
      cname: null
    };
  },
  onLoad: function onLoad(options) {
    // this.mobile = uni.getStorageSync('mobile')
    if (options.basedata) {
      this.basedata = JSON.parse(options.basedata);
    }
    if (uni.getStorageSync('qYsign')) {
      this.qr_com = uni.getStorageSync('qYsign').comId;
      this.qr_are = uni.getStorageSync('qYsign').areaId;
      this.qr_str = uni.getStorageSync('qYsign').streetId;
      this.areaTag = false;
    }
    // this.setLinkList();

    // 获取传递参数 分享情况
    // uni.showToast({title: '信息：'+options.scene})
    if (options.scene) {
      this.scene = decodeURIComponent(options.scene);
      if (this.scene !== 'undefined') {
        var arr = this.scene.split(",");
        this.mid = arr[0];
        this.qr_com = arr[3];
        this.qr_are = arr[1];
        this.qr_str = arr[2];
        this.areaTag = false;
      } else {
        this.scene = '';
      }
      this.shareCount();
    }
    if (uni.getStorageSync('name') != '') {
      var name = uni.getStorageSync('name');
      var avatar = uni.getStorageSync('avatar');
      this.ss = 'sobot|' + name + '|' + avatar;
    }
    this.onGetPhoneNumber();
  },
  methods: {
    updataModal: function updataModal(e) {
      this.modal = e;
    },
    submit: function submit(e) {
      if (e === true) {
        this.register();
      }
    },
    shareCount: function shareCount() {
      uni.request({
        url: this.$store.state.websiteUrl + 'api/scanQRSuccess',
        method: 'GET',
        data: {
          mId: this.mid
        },
        success: function success(res) {}
      });
    },
    onGetPhoneNumber: function onGetPhoneNumber(e) {
      var that = this;
      uni.login({
        provider: 'weixin',
        success: function success(loginRes) {
          if (e.detail.errMsg == 'getPhoneNumber:ok') {
            uni.request({
              url: that.$store.state.websiteUrl + 'api/getPhoneNum',
              data: {
                errMsg: e.detail.errMsg,
                iv: e.detail.iv,
                encryptedData: e.detail.encryptedData,
                code: loginRes.code
              },
              method: 'GET',
              success: function success(res) {
                if (res.data.msg === 'success') {
                  that.openid = res.data.data.openid;
                  that.mobile = res.data.data.purePhoneNumber;
                  that.doRequest();
                } else {
                  uni.showToast({
                    title: '出错了，请再试一次',
                    icon: 'none'
                  });
                }
              }
            });
          } else if (e.detail.errMsg === 'getPhoneNumber:fail user deny') {
            wx.showToast({
              title: '您拒绝了授权',
              icon: 'none',
              duration: 2000
            });
          }
        }
      });
    },
    doRequest: function doRequest() {
      var that = this;
      if (this.basedata !== null) {
        // uni.showModal({
        // 	title: '提示',
        // 	content: `您选择的社区是\r\n\r\n${this.aname}\r\n${this.sname}\r\n${this.cname}\r\n\r\n*<text style:'color:red'>确认后一个月内不能更改</text>`,
        // 	success: res => {
        // 		if (res.confirm) {

        // 			that.register()
        // 		}
        // 	},
        // })
        this.content = "<view>".concat(this.aname ? this.aname : uni.getStorageSync('qYsign').areaName, "</view><view>").concat(this.sname ? this.sname : uni.getStorageSync('qYsign').streetName, "</view><view>").concat(this.cname ? this.cname : uni.getStorageSync('qYsign').comName, "</view>");
        this.modal = true;
      } else {
        that.register();
      }
    },
    register: function register() {
      uni.showLoading({
        title: '请稍等...',
        mask: true
      });
      if (this.name.trim() == '无' || this.name == 'null') {
        uni.showToast({
          title: "名称有误!",
          duration: 2000,
          icon: "none"
        });
        uni.hideLoading();
        return;
      }
      uni.request({
        url: this.$store.state.websiteUrl + 'api/registered',
        method: "POST",
        header: {
          'content-type': 'application/x-www-form-urlencoded'
        },
        data: {
          nickName: this.nickname,
          avatar: this.avatar,
          openid: this.basedata ? this.basedata.openid : this.openid,
          name: this.name,
          mobile: this.basedata ? this.basedata.purePhoneNumber : this.mobile,
          area_id: this.qr_are ? this.qr_are : this.area_id,
          street_id: this.qr_str ? this.qr_str : this.street_id,
          com_id: this.qr_com ? this.qr_com : this.com_id,
          area: this.area,
          mId: this.mid // 记录 推广人ID
        },

        dataType: 'json',
        success: function success(res) {
          console.log(res, 'regiss====>');
          uni.hideLoading();
          uni.showToast({
            title: res.data.msg,
            icon: "none",
            mask: true,
            duration: 2000
          });
          if (res.data.status == 1) {
            uni.setStorageSync('userinfo', res.data.data);
            uni.setStorageSync('cur_id', res.data.data.id);
            uni.setStorageSync("name", res.data.data.username);
            uni.setStorageSync("avatar", res.data.data.avatar);
            uni.setStorageSync('sourcePage', "login");
            uni.setStorageSync('identity', 1);
            uni.setStorageSync('org_manage_flag', 2);
            uni.setStorageSync('login', 1);
            var flag = uni.getStorageSync('user_sign_up_flag');
            if (flag === "1011" || flag === 1012) {
              //通过扫描报名项目二维码,或点击组织身份登录，注册成功后直接跳转到切换身份页面
              // console.info("通过扫描报名项目二维码，居民用户注册成功，跳转到切换身份页面")
              /* if(flag === 1012){
              	uni.removeStorageSync('user_sign_up_flag')
              } */
              setTimeout(function () {
                uni.navigateTo({
                  url: "../about/identity"
                });
                /* uni.reLaunch({
                	url: "../about/identity"
                }) */
              }, 2000);
            } else {
              uni.setStorageSync('login', 1);
              uni.setStorageSync('sourcePage', "login");
              if (uni.getStorageSync('share')) {
                uni.showToast({
                  title: '登录成功，即将前往评价页面',
                  icon: 'none',
                  duration: 1000,
                  success: function success() {
                    setTimeout(function () {
                      uni.navigateTo({
                        url: '../about/projectInfo/pj?id=' + uni.getStorageSync('share')
                      });
                    }, 1000);
                  }
                });
              } else if (uni.getStorageSync('sign')) {
                uni.showToast({
                  title: '登录成功，即将前往报名页面',
                  icon: 'none',
                  duration: 1000,
                  success: function success() {
                    setTimeout(function () {
                      uni.navigateTo({
                        url: '../about/projectInfo/sign?id=' + uni.getStorageSync('sign')
                      });
                    }, 1000);
                  }
                });
              } else if (uni.getStorageSync('qYsign')) {
                uni.showToast({
                  title: '登录成功，即将前往报名页面',
                  icon: 'none',
                  duration: 1000,
                  success: function success() {
                    setTimeout(function () {
                      uni.navigateTo({
                        url: '../qyPages/QYactivitySignUp?id=' + uni.getStorageSync('qYsign').id
                      });
                    }, 1000);
                  }
                });
              } else if (uni.getStorageSync('p1shareInfo')) {
                uni.showToast({
                  title: '登录成功，即将前往民意征集页面',
                  icon: 'none',
                  duration: 1000,
                  success: function success() {
                    setTimeout(function () {
                      uni.navigateTo({
                        url: '../project1/project1_page1?registered=' + res.data.data.id
                      });
                    }, 1000);
                  }
                });
              } else if (uni.getStorageSync('p2shareInfo')) {
                uni.showToast({
                  title: '登录成功，即将前往项目查看页面',
                  icon: 'none',
                  duration: 1000,
                  success: function success() {
                    setTimeout(function () {
                      uni.navigateTo({
                        url: '../project1/project1_page2?registered=' + res.data.data.id
                      });
                    }, 1000);
                  }
                });
              } else if (uni.getStorageSync('shareData')) {
                uni.showToast({
                  title: '登录成功，即将返回',
                  icon: 'none',
                  duration: 1000,
                  success: function success() {
                    setTimeout(function () {
                      var url = uni.getStorageSync('route');
                      uni.navigateTo({
                        url: url,
                        success: function success() {
                          uni.setStorageSync('registerId', res.data.data.id);
                        }
                      });
                    }, 1000);
                  }
                });
              } else {
                setTimeout(function () {
                  uni.switchTab({
                    url: "../index/index"
                  });
                }, 2000);
              }
            }
          }
        }
      });
    },
    lgok: function lgok() {
      uni.switchTab({
        url: "../about/about"
      });
    },
    login: function login() {
      var that = this;
      uni.login({
        provider: 'weixin',
        success: function success(loginRes) {
          that.session_code = loginRes.code;
        }
      });
    },
    // onGetPhoneNumber() {
    // 	let that = this
    // 	uni.request({
    // 		url:that.$store.state.websiteUrl + 'api/getPhoneNum',
    // 		data:{
    // 			errMsg:uni.getStorageSync('msg'),
    // 			iv:uni.getStorageSync('iv'),
    // 			code:uni.getStorageSync('code'),
    // 			encryptedData:uni.getStorageSync('encr')
    // 		},
    // 		method:'GET',
    // 		success: (res) => {
    // 			console.log(res,'res')
    // 			that.mobile = res.data.data
    // 			that.doRequest()
    // 		}
    // 	})
    // uni.login({
    // 	provider: 'weixin',
    // 	success: function(loginRes) {
    // 		if (e.detail.errMsg == 'getPhoneNumber:ok') {
    // 			uni.request({
    // 				url: that.$store.state.websiteUrl + 'api/getPhoneNum',
    // 				data: {
    // 					errMsg: e.detail.errMsg,
    // 					iv: e.detail.iv,
    // 					encryptedData: e.detail.encryptedData,
    // 					code: loginRes.code,
    // 				},
    // 				method: 'GET',
    // 				success: (res) => {
    // 					that.mobile = res.data.data
    // 					that.doRequest()
    // 				}
    // 			})
    // 		} else if (e.detail.errMsg === 'getPhoneNumber:fail user deny') {
    // 			wx.showToast({
    // 				title: '您拒绝了授权',
    // 				icon: 'none',
    // 				duration: 2000
    // 			})
    // 		}
    // 	}
    // });
    // },
    //设置三级联动数据
    setLinkList: function setLinkList() {
      var that = this;
      uni.request({
        url: that.$store.state.websiteUrl + 'common/areaData',
        success: function success(res) {
          if (res.statusCode == 200) {
            that.linkList = res.data;
          } else {
            uni.showToast({
              title: '请求超时，请检查网络或稍后再试',
              mask: true
            });
          }
        }
      });
    },
    toggleTab: function toggleTab(str) {
      if (this.linkList == '') {
        this.setLinkList();
      }
      this.$refs[str].show();
    },
    onConfirm: function onConfirm(val) {
      this.aname = val.obj.col1.label;
      this.sname = val.obj.col2.label;
      this.cname = val.obj.col3.label;
      this.area = val.result;
      this.area_id = val.obj.col1.value;
      this.street_id = val.obj.col2.value;
      this.com_id = val.obj.col3.value;
      uni.setStorageSync('area_id', val.obj.col1.value);
      uni.setStorageSync('street_id', val.obj.col2.value);
      uni.setStorageSync('com_id', val.obj.col3.value);
    },
    onCancel: function onCancel() {},
    registered: function registered() {
      uni.navigateTo({
        url: "./residents"
      });
    }
  }
};
exports.default = _default;
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./node_modules/@dcloudio/uni-mp-weixin/dist/index.js */ 2)["default"], __webpack_require__(/*! ./node_modules/@dcloudio/uni-mp-weixin/dist/wx.js */ 1)["default"]))

/***/ }),

/***/ 67:
/*!************************************************************************************************************************!*\
  !*** C:/Users/13072/Documents/HBuilderProjects/wxpage/pages/registered/residents.vue?vue&type=style&index=0&lang=css& ***!
  \************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _D_HBuilderX_3_8_12_20230817_HBuilderX_plugins_uniapp_cli_node_modules_mini_css_extract_plugin_dist_loader_js_ref_6_oneOf_1_0_D_HBuilderX_3_8_12_20230817_HBuilderX_plugins_uniapp_cli_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_D_HBuilderX_3_8_12_20230817_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_loaders_stylePostLoader_js_D_HBuilderX_3_8_12_20230817_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_preprocess_loader_index_js_ref_6_oneOf_1_2_D_HBuilderX_3_8_12_20230817_HBuilderX_plugins_uniapp_cli_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_3_D_HBuilderX_3_8_12_20230817_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_index_js_vue_loader_options_D_HBuilderX_3_8_12_20230817_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_webpack_uni_mp_loader_lib_style_js_residents_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!./node_modules/mini-css-extract-plugin/dist/loader.js??ref--6-oneOf-1-0!./node_modules/css-loader/dist/cjs.js??ref--6-oneOf-1-1!./node_modules/@dcloudio/vue-cli-plugin-uni/packages/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/@dcloudio/vue-cli-plugin-uni/packages/webpack-preprocess-loader??ref--6-oneOf-1-2!./node_modules/postcss-loader/src??ref--6-oneOf-1-3!./node_modules/@dcloudio/vue-cli-plugin-uni/packages/vue-loader/lib??vue-loader-options!./node_modules/@dcloudio/webpack-uni-mp-loader/lib/style.js!./residents.vue?vue&type=style&index=0&lang=css& */ 68);
/* harmony import */ var _D_HBuilderX_3_8_12_20230817_HBuilderX_plugins_uniapp_cli_node_modules_mini_css_extract_plugin_dist_loader_js_ref_6_oneOf_1_0_D_HBuilderX_3_8_12_20230817_HBuilderX_plugins_uniapp_cli_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_D_HBuilderX_3_8_12_20230817_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_loaders_stylePostLoader_js_D_HBuilderX_3_8_12_20230817_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_preprocess_loader_index_js_ref_6_oneOf_1_2_D_HBuilderX_3_8_12_20230817_HBuilderX_plugins_uniapp_cli_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_3_D_HBuilderX_3_8_12_20230817_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_index_js_vue_loader_options_D_HBuilderX_3_8_12_20230817_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_webpack_uni_mp_loader_lib_style_js_residents_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_D_HBuilderX_3_8_12_20230817_HBuilderX_plugins_uniapp_cli_node_modules_mini_css_extract_plugin_dist_loader_js_ref_6_oneOf_1_0_D_HBuilderX_3_8_12_20230817_HBuilderX_plugins_uniapp_cli_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_D_HBuilderX_3_8_12_20230817_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_loaders_stylePostLoader_js_D_HBuilderX_3_8_12_20230817_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_preprocess_loader_index_js_ref_6_oneOf_1_2_D_HBuilderX_3_8_12_20230817_HBuilderX_plugins_uniapp_cli_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_3_D_HBuilderX_3_8_12_20230817_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_index_js_vue_loader_options_D_HBuilderX_3_8_12_20230817_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_webpack_uni_mp_loader_lib_style_js_residents_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _D_HBuilderX_3_8_12_20230817_HBuilderX_plugins_uniapp_cli_node_modules_mini_css_extract_plugin_dist_loader_js_ref_6_oneOf_1_0_D_HBuilderX_3_8_12_20230817_HBuilderX_plugins_uniapp_cli_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_D_HBuilderX_3_8_12_20230817_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_loaders_stylePostLoader_js_D_HBuilderX_3_8_12_20230817_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_preprocess_loader_index_js_ref_6_oneOf_1_2_D_HBuilderX_3_8_12_20230817_HBuilderX_plugins_uniapp_cli_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_3_D_HBuilderX_3_8_12_20230817_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_index_js_vue_loader_options_D_HBuilderX_3_8_12_20230817_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_webpack_uni_mp_loader_lib_style_js_residents_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _D_HBuilderX_3_8_12_20230817_HBuilderX_plugins_uniapp_cli_node_modules_mini_css_extract_plugin_dist_loader_js_ref_6_oneOf_1_0_D_HBuilderX_3_8_12_20230817_HBuilderX_plugins_uniapp_cli_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_D_HBuilderX_3_8_12_20230817_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_loaders_stylePostLoader_js_D_HBuilderX_3_8_12_20230817_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_preprocess_loader_index_js_ref_6_oneOf_1_2_D_HBuilderX_3_8_12_20230817_HBuilderX_plugins_uniapp_cli_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_3_D_HBuilderX_3_8_12_20230817_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_index_js_vue_loader_options_D_HBuilderX_3_8_12_20230817_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_webpack_uni_mp_loader_lib_style_js_residents_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_D_HBuilderX_3_8_12_20230817_HBuilderX_plugins_uniapp_cli_node_modules_mini_css_extract_plugin_dist_loader_js_ref_6_oneOf_1_0_D_HBuilderX_3_8_12_20230817_HBuilderX_plugins_uniapp_cli_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_D_HBuilderX_3_8_12_20230817_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_loaders_stylePostLoader_js_D_HBuilderX_3_8_12_20230817_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_preprocess_loader_index_js_ref_6_oneOf_1_2_D_HBuilderX_3_8_12_20230817_HBuilderX_plugins_uniapp_cli_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_3_D_HBuilderX_3_8_12_20230817_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_index_js_vue_loader_options_D_HBuilderX_3_8_12_20230817_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_webpack_uni_mp_loader_lib_style_js_residents_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ 68:
/*!****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/mini-css-extract-plugin/dist/loader.js??ref--6-oneOf-1-0!./node_modules/css-loader/dist/cjs.js??ref--6-oneOf-1-1!./node_modules/@dcloudio/vue-cli-plugin-uni/packages/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/@dcloudio/vue-cli-plugin-uni/packages/webpack-preprocess-loader??ref--6-oneOf-1-2!./node_modules/postcss-loader/src??ref--6-oneOf-1-3!./node_modules/@dcloudio/vue-cli-plugin-uni/packages/vue-loader/lib??vue-loader-options!./node_modules/@dcloudio/webpack-uni-mp-loader/lib/style.js!C:/Users/13072/Documents/HBuilderProjects/wxpage/pages/registered/residents.vue?vue&type=style&index=0&lang=css& ***!
  \****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin
    if(false) { var cssReload; }
  

/***/ })

},[[61,"common/runtime","common/vendor"]]]);
//# sourceMappingURL=../../../.sourcemap/mp-weixin/pages/registered/residents.js.map