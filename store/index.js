import Vue from 'vue'
import Vuex from "vuex"

Vue.use(Vuex)

const store = new Vuex.Store({
	state: {
		// websiteUrl : 'http://localhost:9010/server/' , //测试服地址
		// websiteUrl: 'https://www.chengdupb.cn/server/', //正式服地址
		// websiteUrl : 'http://192.168.4.155:9010/server/' , //测试服地址、
		// websiteUrl : 'http://192.168.4.108:9010/server/' , //测试服地址、
		websiteUrl : 'https://demo.chengdupb.cn/server/',//测试服地址
		//websiteUrl : 'https://cdpre.tfsmy.com/server/',//天府市民云测试服地址
		// websiteUrl : 'https://tfsmy.chengdu.gov.cn/server/',//天府市民云正式服地址
		qyWebUrl:'https://demo.chengdupb.cn/server/dz/', //青羊区定制请求地址
		// qyWebUrl:'https://cdpre.tfsmy.com/server/dz/', //天府市民云青羊区定制请求地址
		// qyWebUrl:'https://tfsmy.chengdu.gov.cn/server/dz/', //天府市民云青羊区定制请求地址
		// qyWebUrl : 'http://192.168.4.108:9020/server/dz/' , //青羊区定制请求地址、
		// qyWebUrl: 'https://www.chengdupb.cn/server/dz/',
		modal: false,
		timer: null
	},
	mutations: {
		updata(state, data) {
			state.timer = data
		}
	}
});

export default store
