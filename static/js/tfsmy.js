(function() {
    function eshimin() {
        this.connectWebViewJavascriptBridge = function(callback) {
            if (window.WebViewJavascriptBridge) {
                callback(WebViewJavascriptBridge);
            } else {
                document.addEventListener("WebViewJavascriptBridgeReady", function() {
                    callback(WebViewJavascriptBridge);
                }, false);
            }
        };
        this.connectWebViewJavascriptBridge(function(bridge) {
            bridge.init(function(message, responseCallback) {
                console.log("JS got a message", message);
                var data = {
                    "Javascript Responds":"huidao!"
                };
                console.log("JS responding with", data);
                responseCallback(data);
            });
        });
    }
    eshimin.prototype = {
        ready:function(callback) {},
        error:function(callback) {},
        closeWindow:function(options) {
            var ua = navigator.userAgent.toLowerCase();
            if (ua.indexOf('baidu') > -1) {
                loadScript("https://b.bdstatic.com/searchbox/icms/searchbox/js/swan-2.0.4.js", function () {
                    swan.webView.navigateBack({
                        success: function (res) {
                            console.log("closeWindow res:" + JSON.stringify(res));
                            if (options && options.onResponse) {
                                options.onResponse(res);
                            }
                        },
                        fail: function (err) {
                            console.log(JSON.stringify(err));
                        }
                    });
                })
            } else if (ua.indexOf('micromessenger') > -1) {
                loadScript('https://res.wx.qq.com/open/js/jweixin-1.3.2.js', function () {
                    wx.miniProgram.navigateBack({
                        delta: 1,
                        success: function(res) {
                            console.log("closeWindow res:" + JSON.stringify(res));
                            if (options && options.onResponse) {
                                options.onResponse(res);
                            }
                        },
                        fail: function(err) {
                            console.log(JSON.stringify(err));
                        }
                    })
                })
            } else if (isAlipayMp()) {
                loadScript('https://appx/web-view.min.js', function () {
                    my.navigateBack({
                        delta: 1,
                        success: function(res) {
                            console.log("closeWindow res:" + JSON.stringify(res));
                            if (options && options.onResponse) {
                                options.onResponse(res);
                            }
                        },
                        fail: function(err) {
                            console.log(JSON.stringify(err))
                        }
                    })
                })
            }else {
                window.WebViewJavascriptBridge.callHandler("closeWindow", "", function(responseData) {
                    console.log("closeWindow responseData:" + responseData);
                    if (options && options.onResponse) {
                        options.onResponse(responseData);
                    }
                });
            }
        },
        setTitle:function(options) {
	    console.log(options);
            if (!isMp()) {
                window.WebViewJavascriptBridge.callHandler("setTitle", encodeURIComponent(options.title), function(responseData) {
                    console.log("setTitle responseData:" + responseData);
                    if (options && options.onResponse) {
                        options.onResponse(responseData);
                    }
                });
            }
        },
        setTitleColor:function(options) {
            window.WebViewJavascriptBridge.callHandler("setTitleColor", options.titleColor, function(responseData) {
                console.log("setTitleColor responseData:" + responseData);
                if (options && options.onResponse) {
                    options.onResponse(responseData);
                }
            });
        },
        getNetworkStatus:function(options) {
            window.WebViewJavascriptBridge.callHandler("getNetworkStatus", "", function(responseData) {
                console.log("getNetworkStatus responseData:" + responseData);
                if (options && options.onResponse) {
                    options.onResponse(responseData);
                }
            });
        },
        phoneCall:function(options) {
            window.WebViewJavascriptBridge.callHandler("phoneCall", options.num, function(responseData) {
                console.log("phoneCall responseData:" + responseData);
                if (options && options.onResponse) {
                    options.onResponse(responseData);
                }
            });
        },
        getLocalPicture:function(options) {
            window.WebViewJavascriptBridge.callHandler("getLocalPicture", "", function(responseData) {
                console.log("getLocalPicture responseData:" + responseData);
                if (options && options.onResponse) {
                    options.onResponse(responseData);
                }
            });
        },
        getPictureByCamera:function(options) {
            window.WebViewJavascriptBridge.callHandler("getPictureByCamera", "", function(responseData) {
                console.log("getPictureByCamera responseData:" + responseData);
                if (options && options.onResponse) {
                    options.onResponse(responseData);
                }
            });
        },
        chooseImage:function(options) {
            if (!isMp()) {
                window.WebViewJavascriptBridge.callHandler("chooseImage", "", function(responseData) {
                    console.log("chooseImage responseData:" + responseData);
                    if (options && options.onResponse) {
                        options.onResponse(responseData);
                    }
                });
            }
        },
        getAddress:function(options) {
            if (!isMp()) {
                window.WebViewJavascriptBridge.callHandler("getAddress", "", function(responseData) {
                    console.log("getAddress responseData:" + responseData);
                    if (options && options.onResponse) {
                        options.onResponse(responseData);
                    }
                });
            }
        },
        getLocation:function(options) {
            var ua = navigator.userAgent.toLowerCase();
            if (ua.indexOf('baidu') > -1) {
                loadScript("https://b.bdstatic.com/searchbox/icms/searchbox/js/swan-2.0.4.js", function () {
                    swan.getLocation({
                        type: 'gcj02',
                        success: function (res) {
                            if (options && options.onResponse) {
                                var ret = {
                                	code: "200",
                                	msg: "success",
                                	longitude: res.longitude,
                                	latitude: res.latitude
                                }
                                ret = JSON.stringify(ret)
                                options.onResponse(ret);
                            }
                        },
                        fail: function (err) {
                            console.log('错误码：' + err.errCode);
                            console.log('错误信息：' + err.errMsg);
                        }
                    });
                });
            } else if (ua.indexOf('micromessenger') > -1) {
                loadScript("https://res.wx.qq.com/open/js/jweixin-1.3.2.js", function () {
                    wx.getLocation({
                        type: 'wgs84',
                        altitude: 'false',
                        success: function(res) {
                            if (options && options.onResponse) {
                                var ret = {
                                    code: "200",
                                    msg: "success",
                                    longitude: res.longitude,
                                    latitude: res.latitude
                                }
                                ret = JSON.stringify(ret)
                                options.onResponse(ret);
                            }
                        },
                        fail: function (err) {
                            console.log(JSON.stringify(err));
                        },
                        complete: function (com) {
                        }
                    })
                });
            } else if (isAlipayMp()) {
                loadScript("https://appx/web-view.min.js", function () {
                    my.getLocation({
                        success: function(res) {
                            if (options && options.onResponse) {
                                var ret = {
                                    code: "200",
                                    msg: "success",
                                    longitude: res.longitude,
                                    latitude: res.latitude
                                }
                                ret = JSON.stringify(ret)
                                options.onResponse(ret);
                            }
                        }
                    })
                });
            } else {
                window.WebViewJavascriptBridge.callHandler("getLocation", "", function(responseData) {
                    console.log("getLocation responseData:" + responseData);
                    if (options && options.onResponse) {
                        options.onResponse(responseData);
                    }
                });
            }
        },
        startShake:function(options) {
            window.WebViewJavascriptBridge.callHandler("startShake", "", function(responseData) {
                console.log("startShake responseData:" + responseData);
                if (options && options.onResponse) {
                    options.onResponse(responseData);
                }
            });
        },
        getBarcode:function(options) {
            window.WebViewJavascriptBridge.callHandler("getBarcode", "", function(responseData) {
                console.log("getBarcode responseData:" + responseData);
                if (options && options.onResponse) {
                    options.onResponse(responseData);
                }
            });
        },
        share:function(options) {
        	var ua = navigator.userAgent.toLowerCase();
            if (ua.indexOf('baidu') > -1) {
                loadScript("https://b.bdstatic.com/searchbox/icms/searchbox/js/swan-2.0.4.js", function () {
                    swan.openShare({
                        title: '天府市民云',
					    content: '一站式互联网+城市服务平台',
					    path: '/pages/index/index',
                        success: function (res) {
                            if (options && options.onResponse) {
                                options.onResponse(res);
                            }
                        },
                        fail: function (err) {
                            console.log('错误码：' + err.errCode);
                            console.log('错误信息：' + err.errMsg);
                        }
                    });
                });
            } if (isAlipayMp()) {
                loadScript("https://appx/web-view.min.js", function () {
                    my.alert({
                        title: '亲',
                        content: '请点击右上角选项进行分享哦',
                        buttonText: '我知道了',
                        success: function() {
                        },
                    });
                });
            } else {
                if (options.version) {
                }else{
                    options.version = '0';
                }
            	var string = '{"title":"' + options.title + '",' + '"content":"' + options.content + '",' + '"imgUrl":"' + options.imgUrl + '",' + '"link":"' + options.link + '",' + '"login":"' + options.login + '",' + '"version":"' + options.version + '",' + '"platformType":' + JSON.stringify(options.platformType) + "}";
	            window.WebViewJavascriptBridge.callHandler("share", encodeURIComponent(string), function(responseData) {
	                console.log("share responseData:" + responseData);
	                if (options && options.onResponse) {
	                    options.onResponse(responseData);
	                }
	            });
            }
        },
        WXPay:function(options) {
            var string = '{"data":"' + options.data + '"}';
            window.WebViewJavascriptBridge.callHandler("WXPay", encodeURIComponent(string), function(responseData) {
                console.log("WXPay responseData:" + responseData);
                if (options && options.onResponse) {
                    options.onResponse(responseData);
                }
            });
        },
        getUserInfo:function(options) {
            if (!isMp()) {
                window.WebViewJavascriptBridge.callHandler("getUserInfo", "", function(responseData) {
                    console.log("getBarcode responseData:" + responseData);
                    if (options && options.onResponse) {
                        options.onResponse(responseData);
                    }
                });
            }
        },
        realPersonAuth:function(options) {
            if (!isMp()) {
                var string = '{"clientId":"' + options.clientId + '",' + '"token":"' + options.token + '",' + '"authType":"' + options.authType + '"}';
                window.WebViewJavascriptBridge.callHandler("realPersonAuth", encodeURIComponent(string), function(responseData) {
                    if (options && options.onResponse) {
                        options.onResponse(responseData);
                    }
                });
            }
        },
        saveImage:function(options) {
            var string = '{"imgStr":"' + options.imgStr + '"}';
            window.WebViewJavascriptBridge.callHandler("saveImage", encodeURIComponent(string), function(responseData) {
                if (options && options.onResponse) {
                    options.onResponse(responseData);
                }
            });
        },
        getDeviceId:function(options) {
            if (!isMp()) {
                var string = '{"native":"' + options.native + '"}';
                window.WebViewJavascriptBridge.callHandler("getDeviceId", encodeURIComponent(string), function(responseData) {
                    console.log("getDeviceId responseData:" + responseData);
                    if (options && options.onResponse) {
                        options.onResponse(responseData);
                    }
                });
            }
        },
        setScreenHighlight:function(options) {
            window.WebViewJavascriptBridge.callHandler("setScreenHighlight", "", function(responseData) {
                console.log("setScreenHighlight responseData:" + responseData);
                if (options && options.onResponse) {
                    options.onResponse(responseData);
                }
            });
        },
        openWebUrl:function(options) {
            var string = '{"webUrl":"' + options.webUrl + '",' + '"title":"' + options.title + '"}';
            window.WebViewJavascriptBridge.callHandler("openWebUrl", encodeURIComponent(string), function(responseData) {
                console.log("openWebUrl responseData:" + responseData);
                if (options && options.onResponse) {
                    options.onResponse(responseData);
                }
            });
        },
        resetScreenBrightness:function(options) {
            window.WebViewJavascriptBridge.callHandler("resetScreenBrightness", "", function(responseData) {
                console.log("resetScreenBrightness responseData:" + responseData);
                if (options && options.onResponse) {
                    options.onResponse(responseData);
                }
            });
        },
        setCityinfo:function(options) {
            var string = '{"cityCode":"' + options.cityCode + '",' + '"cityName":"' + options.cityName + '",' + '"cityNewsTitle":"' + options.cityNewsTitle + '",' + '"cityParent":"' + options.cityParent + '"}';
            window.WebViewJavascriptBridge.callHandler("setCityinfo", encodeURIComponent(string), function(responseData) {
                console.log("setCityinfo responseData:" + responseData);
                if (options && options.onResponse) {
                    options.onResponse(responseData);
                }
            });
        },
        getCityCode:function(options) {
            if(!isMp()) {
                window.WebViewJavascriptBridge.callHandler("getCityCode", "", function(responseData) {
                    console.log("getCityCode responseData:" + responseData);
                    if (options && options.onResponse) {
                        options.onResponse(responseData);
                    }
                });
            }
        },
	electronicOcr:function(options) {
            window.WebViewJavascriptBridge.callHandler("electronicOcr", "", function(responseData) {
                console.log("electronicOcr responseData:" + responseData);
                if (options && options.onResponse) {
                    options.onResponse(responseData);
                }
            });
        },
        /**
 *          * 打开实名认证方式列表
 *                   *
 *                            * @return code：200 成功，201 取消
 *                                     */
        openRealNameAuthenticationList:function(options) {
            window.WebViewJavascriptBridge.callHandler("openRealNameAuthenticationList", "", function(responseData) {
                console.log("openRealNameAuthenticationList responseData:" + responseData);
                if (options && options.onResponse) {
                    options.onResponse(responseData);
                }
            });
        },
        /**
 *          * 人脸识别，实名认证
 *                   *
 *                            * @param token     用户信息标识
 *                                     * @param channelId 渠道号
 *                                              *
 *                                                       * @return code：200 成功，201 取消，500 错误
 *                                                                *         msg： 对 code 的一般描述
 *                                                                         */
        realNameAuthentication:function(options) {
            var string = '{"token":"' + (options.token ? options.token : '') + '",' +
                    '"channelId":"' + (options.channelId ? options.channelId : '') + '"}'
            window.WebViewJavascriptBridge.callHandler(
                "realNameAuthentication",
                encodeURIComponent(string),
                function(responseData) {
                    console.log("realNameAuthentication responseData:" + responseData);
                    if (options && options.onResponse) {
                        options.onResponse(responseData);
                    }
                }
            );
        },
        /**
 *          * 人脸识别，实人认证，仅在用户已实名时才有用
 *                   *
 *                            * @param token
 *                                     * @param clientId
 *                                              *
 *                                                       * @return code：200 成功，201 取消，500 错误
 *                                                                *         msg： 对 code 的一般描述
 *                                                                         */
        facialIdentification:function(options) {
            var string = '{"token":"' + (options.token ? options.token : '') + '",' +
                    '"clientId":"' + (options.clientId ? options.clientId : '') + '"}'
            window.WebViewJavascriptBridge.callHandler(
                "facialIdentification",
                encodeURIComponent(string),
                function(responseData) {
                    console.log("facialIdentification responseData:" + responseData);
                    if (options && options.onResponse) {
                        options.onResponse(responseData);
                    }
                }
            );
        },
        showLoading:function(options) {
            window.WebViewJavascriptBridge.callHandler("showLoading", "", function(responseData) {
                console.log("showLoading responseData:" + responseData);
                if (options && options.onResponse) {
                    options.onResponse(responseData);
                }
            });
        },
        hideLoading:function(options) {
            window.WebViewJavascriptBridge.callHandler("hideLoading", "", function(responseData) {
                console.log("hideLoading responseData:" + responseData);
                if (options && options.onResponse) {
                    options.onResponse(responseData);
                }
            });
        },
	getScreenShotImage:function (options){
             window.WebViewJavascriptBridge.callHandler(
                "getScreenShotImage"
                ,""
                ,function (responseData) {
                    if(options && options.onResponse){
                         options.onResponse(responseData);
                    }
                }
           );
         },
	getEESBCardSign: function (options) {
            var string = '{\"domainStr\":\"'+options.domainStr+'\",'
                    +'\"signStr\":\"'+options.signStr+'\"}';
            window.WebViewJavascriptBridge.callHandler(
                'getEESBCardSign'
                ,encodeURIComponent(string)
                ,function (responseData) {
                    if(options && options.onResponse){
                        options.onResponse(responseData);
                    }
                }
            );
        },
	getPHAuthorization:function(options) {
            window.WebViewJavascriptBridge.callHandler("getPHAuthorization", "", function(responseData) {
                if (options && options.onResponse) {
                    options.onResponse(responseData);
                }
            });
        },
	liveAuthentication:function(options) {
            var string = '{"token":"' + (options.token ? options.token : '') + '",' + '"channelId":"' + (options.channelId ? options.channelId : '') + '"}'
            window.WebViewJavascriptBridge.callHandler(
                "liveAuthentication",
                encodeURIComponent(string),
                function(responseData) {
                    console.log("liveAuthentication responseData:" + responseData);
                    if (options && options.onResponse) {
                        options.onResponse(responseData);
                    }
                }
            );
        },
        //政融支付(维修资金)
    CCBGovPay:function(options) {
            var string = '{"payTypeUrl":"' + options.payTypeUrl + '"}';
            window.WebViewJavascriptBridge.callHandler("CCBGovPay", encodeURIComponent(string), function(responseData) {
                console.log("CCBGovPay responseData:" + responseData);
                if (options && options.onResponse) {
                    options.onResponse(responseData);
                }
            });
        },
        //获取版本号
        getAppVersion:function(options) {
            window.WebViewJavascriptBridge.callHandler("getAppVersion", "", function(responseData) {
                console.log("getAppVersion responseData:" + responseData);
                if (options && options.onResponse) {
                    options.onResponse(responseData);
                }
            });
        },
        //引导用户登录
        userLogin:function(options) {
            window.WebViewJavascriptBridge.callHandler("userLogin", "", function(responseData) {
                console.log("userLogin responseData:" + responseData);
                if (options && options.onResponse) {
                    options.onResponse(responseData);
                }
            });
        },
        //引导用户实名认证
        userIdentityAuth:function(options) {
            window.WebViewJavascriptBridge.callHandler("userIdentityAuth", "", function(responseData) {
                console.log("userIdentityAuth responseData:" + responseData);
                if (options && options.onResponse) {
                    options.onResponse(responseData);
                }
            });
        },
        //获取用户账号状态
        getUserStatus:function(options) {
            window.WebViewJavascriptBridge.callHandler("getUserStatus", "", function(responseData) {
                console.log("getUserStatus responseData:" + responseData);
                if (options && options.onResponse) {
                    options.onResponse(responseData);
                }
            });
        },

        //开始登录
    startLogin:function(options) {
            var string = '{"redirectUrl":"' + options.redirectUrl + '"}';
            window.WebViewJavascriptBridge.callHandler("startLogin", encodeURIComponent(string), function(responseData) {
                console.log("startLogin responseData:" + responseData);
                if (options && options.onResponse) {
                    options.onResponse(responseData);
                }
            });
        },
        //获取支付宝和微信的授权code,
        //入参:AuthCode为:WXAuth(微信)、aplipayAuth(支付宝)
        //出参:authCode(授权码)
        getBindingMobileAuthCode:function(options) {
            var string = '{"authType":"' + options.authType + '"}';
            window.WebViewJavascriptBridge.callHandler("getBindingMobileAuthCode", encodeURIComponent(string), function(responseData) {
                console.log("getBindingMobileAuthCode responseData:" + responseData);
                if (options && options.onResponse) {
                    options.onResponse(responseData);
                }
            });
        },
        //开始语音识别输入文字
        startRecord:function(options) {
            window.WebViewJavascriptBridge.callHandler("startRecord", "", function(responseData) {
                console.log("startRecord responseData:" + responseData);
                if (options && options.onResponse) {
                    options.onResponse(responseData);
                }
            });
        },
        //结束语音识别并返回识别结果
        translateVoice:function(options) {
            window.WebViewJavascriptBridge.callHandler("translateVoice", "", function(responseData) {
                console.log("translateVoice responseData:" + responseData);
                if (options && options.onResponse) {
                    options.onResponse(responseData);
                }
            });
        },

        //景泰保险支付
        ElectronicPay:function(options) {
            var string = '{"payInfo":"' + options.payInfo + '"}';
            window.WebViewJavascriptBridge.callHandler("ElectronicPay", encodeURIComponent(string), function(responseData) {
                console.log("ElectronicPay responseData:" + responseData);
                if (options && options.onResponse) {
                    options.onResponse(responseData);
                }
            });
        },
        //跳转到其他城市门户
        jumpToOtherCity:function(options) {
            var string = '{"cityCode":"' + options.cityCode + '",' + '"cityName":"' + options.cityName + '"}';
            window.WebViewJavascriptBridge.callHandler("jumpToOtherCity", encodeURIComponent(string), function(responseData) {
                console.log("jumpToOtherCity responseData:" + responseData);
                if (options && options.onResponse) {
                    options.onResponse(responseData);
                }
            });
        },

        //天府通扫码乘车获取余额
        getTftBalance:function(options) {
            var string = '{"token":"' + options.token + '"}';
            window.WebViewJavascriptBridge.callHandler("getTftBalance", encodeURIComponent(string), function(responseData) {
                console.log("getTftBalance responseData:" + responseData);
                if (options && options.onResponse) {
                    options.onResponse(responseData);
                }
            });
        },

        //天府通扫码乘车去充值
        tftRecharge:function(options) {
            var string = '{"token":"' + options.token + '"}';
            window.WebViewJavascriptBridge.callHandler("tftRecharge", encodeURIComponent(string), function(responseData) {
                console.log("tftRecharge responseData:" + responseData);
                if (options && options.onResponse) {
                    options.onResponse(responseData);
                }
            });
        },
        //打开服务
        openService:function(options) {
            var string = '{"serviceCode":"' + options.serviceCode + '"}';
            window.WebViewJavascriptBridge.callHandler("openService", encodeURIComponent(string), function(responseData) {
                console.log("openService responseData:" + responseData);
                if (options && options.onResponse) {
                    options.onResponse(responseData);
                }
            });
        },
        //市民云支付
        smypay:function(options) {
            var string = '{"channel":"' + options.channel + '",' + '"orderStr":"' + options.orderStr + '"}';
            window.WebViewJavascriptBridge.callHandler("smypay", encodeURIComponent(string), function(responseData) {
                console.log("smypay responseData:" + responseData);
                if (options && options.onResponse) {
                    options.onResponse(responseData);
                }
            });
        },
        //获取城市
        getCity:function(options) {
            window.WebViewJavascriptBridge.callHandler("getCity", "", function(responseData) {
                console.log("getCity responseData:" + responseData);
                if (options && options.onResponse) {
                    options.onResponse(responseData);
                }
            });
        },

        //生活圈选择tab
        homeLifeCircleSelectTab:function(options) {
            var string = '{"type":"' + options.type + '",' + '"url":"' + options.url + '"}';
            window.WebViewJavascriptBridge.callHandler("homeLifeCircleSelectTab", encodeURIComponent(string), function(responseData) {
                console.log("homeLifeCircleSelectTab responseData:" + responseData);
                if (options && options.onResponse) {
                    options.onResponse(responseData);
                }
            });
        },

        //城市漫游使用
    openRoamingService:function(options) {
            var string = '{"cityCode":"' + options.cityCode + '",' + '"serviceCode":"' + options.serviceCode + '"}'
            window.WebViewJavascriptBridge.callHandler(
                "openRoamingService", 
                encodeURIComponent(string), 
                function(responseData) {
                console.log("openRoamingService responseData:" + responseData);
                if (options && options.onResponse) {
                    options.onResponse(responseData);
                    }
                }
            );
        }
    }
    window.eshimin = new eshimin();
    if (window.WebViewJavascriptBridge) {
        return;
    }
    var messagingIframe;
    var sendMessageQueue = [];
    var receiveMessageQueue = [];
    var messageHandlers = {};
    var CUSTOM_PROTOCOL_SCHEME = "yy";
    var QUEUE_HAS_MESSAGE = "__QUEUE_MESSAGE__/";
    var responseCallbacks = {};
    var uniqueId = 1;
    function _createQueueReadyIframe(doc) {
        messagingIframe = doc.createElement("iframe");
        messagingIframe.style.display = "none";
        doc.documentElement.appendChild(messagingIframe);
    }
    function isAndroid() {
        var ua = navigator.userAgent.toLowerCase();
        var isA = ua.indexOf("android") > -1;
        if (isA) {
            return true;
        }
        return false;
    }
    function isIphone() {
        var ua = navigator.userAgent.toLowerCase();
        var isIph = ua.indexOf("iphone") > -1;
        if (isIph) {
            return true;
        }
        return false;
    }
    function isAlipayMp() {
        var ua = navigator.userAgent.toLowerCase();
        var isalipay = ua.indexOf("alipayclient") > -1 && ua.indexOf("miniprogram") > -1;
        if (isalipay) {
            return true;
        }
        return false;
    }
    function isMp() {
        var ua = navigator.userAgent.toLowerCase();
        var isTfsmy = ua.indexOf("tfsmy") > -1;
        if (!isTfsmy) {
            return true;
        }
        return false;
    }
    function init(messageHandler) {
        if (WebViewJavascriptBridge._messageHandler) {
            throw new Error("WebViewJavascriptBridge.init called twice");
        }
        WebViewJavascriptBridge._messageHandler = messageHandler;
        var receivedMessages = receiveMessageQueue;
        receiveMessageQueue = null;
        for (var i = 0; i < receivedMessages.length; i++) {
            _dispatchMessageFromNative(receivedMessages[i]);
        }
    }
    function send(data, responseCallback) {
        _doSend({
            data:data
        }, responseCallback);
    }
    function registerHandler(handlerName, handler) {
        messageHandlers[handlerName] = handler;
    }
    function callHandler(handlerName, data, responseCallback) {
        _doSend({
            handlerName:handlerName,
            data:data
        }, responseCallback);
    }
    function _doSend(message, responseCallback) {
        if (responseCallback) {
            var callbackId = "cb_" + uniqueId++ + "_" + new Date().getTime();
            responseCallbacks[callbackId] = responseCallback;
            message.callbackId = callbackId;
        }
        sendMessageQueue.push(message);
        messagingIframe.src = CUSTOM_PROTOCOL_SCHEME + "://" + QUEUE_HAS_MESSAGE;
    }
    function _fetchQueue() {
        var messageQueueString = JSON.stringify(sendMessageQueue);
        sendMessageQueue = [];
        if (isIphone()) {
            return messageQueueString;
        } else {
            if (isAndroid()) {
                messagingIframe.src = CUSTOM_PROTOCOL_SCHEME + "://return/_fetchQueue/" + messageQueueString;
            }
        }
    }
    function _dispatchMessageFromNative(messageJSON) {
        setTimeout(function() {
            var message = JSON.parse(messageJSON);
            var responseCallback;
            if (message.responseId) {
                responseCallback = responseCallbacks[message.responseId];
                if (!responseCallback) {
                    return;
                }
                responseCallback(message.responseData);
                delete responseCallbacks[message.responseId];
            } else {
                if (message.callbackId) {
                    var callbackResponseId = message.callbackId;
                    responseCallback = function(responseData) {
                        _doSend({
                            responseId:callbackResponseId,
                            responseData:responseData
                        });
                    };
                }
                var handler = WebViewJavascriptBridge._messageHandler;
                if (message.handlerName) {
                    handler = messageHandlers[message.handlerName];
                }
                try {
                    handler(message.data, responseCallback);
                } catch (exception) {
                    if (typeof console != "undefined") {
                        console.log("WebViewJavascriptBridge: WARNING: javascript handler threw.", message, exception);
                    }
                }
            }
        });
    }
    function _handleMessageFromNative(messageJSON) {
        if (receiveMessageQueue) {
            receiveMessageQueue.push(messageJSON);
        } else {
            _dispatchMessageFromNative(messageJSON);
        }
    }
    var WebViewJavascriptBridge = window.WebViewJavascriptBridge = {
        init:init,
        send:send,
        registerHandler:registerHandler,
        callHandler:callHandler,
        _fetchQueue:_fetchQueue,
        _handleMessageFromNative:_handleMessageFromNative
    };
    var doc = document;
    if(!isMp()) {
        _createQueueReadyIframe(doc);
    }
    var readyEvent = doc.createEvent("Events");
    readyEvent.initEvent("WebViewJavascriptBridgeReady");
    readyEvent.bridge = WebViewJavascriptBridge;
    doc.dispatchEvent(readyEvent);

    function loadScript(url, callback){
        if (typeof my !='undefined' || typeof wx !='undefined' || typeof swan !='undefined') {
            console.log('已加载该js');
        } else {
            var script = document.createElement("script");
            script.type = "text/javascript";
            if (script.readyState){ //IE
                script.onreadystatechange = function(){
                    if (script.readyState == "loaded" || script.readyState == "complete"){
                        script.onreadystatechange = null;
                        callback();
                    }
                };
            } else { //Others
                script.onload = function(){
                    callback();
                };
            }
            script.src = url;
            var heads = document.getElementsByTagName("head");
            if(heads.length) {
                heads[0].appendChild(script);
            } else {
                document.body.appendChild(script);
            }
        }
    }
})();