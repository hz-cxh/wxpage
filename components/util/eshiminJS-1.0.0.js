(function() {
    function eshimin() {
        this.connectWebViewJavascriptBridge = function(callback) {
            if (window.WebViewJavascriptBridge) {
                callback(WebViewJavascriptBridge);
            } else {
                document.addEventListener("WebViewJavascriptBridgeReady", function() {
                    callback(WebViewJavascriptBridge);
                }, false);
            }
        };
        this.connectWebViewJavascriptBridge(function(bridge) {
            bridge.init(function(message, responseCallback) {
                console.log("JS got a message", message);
                var data = {
                    "Javascript Responds":"huidao!"
                };
                console.log("JS responding with", data);
                responseCallback(data);
            });
        });
    }
    eshimin.prototype = {
        ready:function(callback) {},
        error:function(callback) {},
        closeWindow:function(options) {
            var ua = navigator.userAgent.toLowerCase();
            if (ua.indexOf('baidu') > -1) {
                loadScript("https://b.bdstatic.com/searchbox/icms/searchbox/js/swan-2.0.4.js", function () {
                    swan.webView.navigateBack({
                        success: function (res) {
                            console.log("closeWindow res:" + JSON.stringify(res));
                            if (options && options.onResponse) {
                                options.onResponse(res);
                            }
                        },
                        fail: function (err) {
                            console.log(JSON.stringify(err));
                        }
                    });
                })
            } else if (ua.indexOf('micromessenger') > -1) {
                loadScript('https://res.wx.qq.com/open/js/jweixin-1.3.2.js', function () {
                    wx.miniProgram.navigateBack({
                        delta: 1,
                        success: function(res) {
                            console.log("closeWindow res:" + JSON.stringify(res));
                            if (options && options.onResponse) {
                                options.onResponse(res);
                            }
                        },
                        fail: function(err) {
                            console.log(JSON.stringify(err));
                        }
                    })
                })
            } else if (isAlipayMp()) {
                loadScript('https://appx/web-view.min.js', function () {
                    my.navigateBack({
                        delta: 1,
                        success: function(res) {
                            console.log("closeWindow res:" + JSON.stringify(res));
                            if (options && options.onResponse) {
                                options.onResponse(res);
                            }
                        },
                        fail: function(err) {
                            console.log(JSON.stringify(err))
                        }
                    })
                })
            }else {
                window.WebViewJavascriptBridge.callHandler("closeWindow", "", function(responseData) {
                    console.log("closeWindow responseData:" + responseData);
                    if (options && options.onResponse) {
                        options.onResponse(responseData);
                    }
                });
            }
        },
        setTitle:function(options) {
	    console.log(options);
            if (!isMp()) {
                window.WebViewJavascriptBridge.callHandler("setTitle", encodeURIComponent(options.title), function(responseData) {
                    console.log("setTitle responseData:" + responseData);
                    if (options && options.onResponse) {
                        options.onResponse(responseData);
                    }
                });
            }
        },
        setTitleColor:function(options) {
            window.WebViewJavascriptBridge.callHandler("setTitleColor", options.titleColor, function(responseData) {
                console.log("setTitleColor responseData:" + responseData);
                if (options && options.onResponse) {
                    options.onResponse(responseData);
                }
            });
        },
        getNetworkStatus:function(options) {
            window.WebViewJavascriptBridge.callHandler("getNetworkStatus", "", function(responseData) {
                console.log("getNetworkStatus responseData:" + responseData);
                if (options && options.onResponse) {
                    options.onResponse(responseData);
                }
            });
        },
        phoneCall:function(options) {
            window.WebViewJavascriptBridge.callHandler("phoneCall", options.num, function(responseData) {
                console.log("phoneCall responseData:" + responseData);
                if (options && options.onResponse) {
                    options.onResponse(responseData);
                }
            });
        },
        getLocalPicture:function(options) {
            window.WebViewJavascriptBridge.callHandler("getLocalPicture", "", function(responseData) {
                console.log("getLocalPicture responseData:" + responseData);
                if (options && options.onResponse) {
                    options.onResponse(responseData);
                }
            });
        },
        getPictureByCamera:function(options) {
            window.WebViewJavascriptBridge.callHandler("getPictureByCamera", "", function(responseData) {
                console.log("getPictureByCamera responseData:" + responseData);
                if (options && options.onResponse) {
                    options.onResponse(responseData);
                }
            });
        },
        chooseImage:function(options) {
            if (!isMp()) {
                window.WebViewJavascriptBridge.callHandler("chooseImage", "", function(responseData) {
                    console.log("chooseImage responseData:" + responseData);
                    if (options && options.onResponse) {
                        options.onResponse(responseData);
                    }
                });
            }
        },
        getAddress:function(options) {
            if (!isMp()) {
                window.WebViewJavascriptBridge.callHandler("getAddress", "", function(responseData) {
                    console.log("getAddress responseData:" + responseData);
                    if (options && options.onResponse) {
                        options.onResponse(responseData);
                    }
                });
            }
        },
        getLocation:function(options) {
            var ua = navigator.userAgent.toLowerCase();
            if (ua.indexOf('baidu') > -1) {
                loadScript("https://b.bdstatic.com/searchbox/icms/searchbox/js/swan-2.0.4.js", function () {
                    swan.getLocation({
                        type: 'gcj02',
                        success: function (res) {
                            if (options && options.onResponse) {
                                var ret = {
                                	code: "200",
                                	msg: "success",
                                	longitude: res.longitude,
                                	latitude: res.latitude
                                }
                                ret = JSON.stringify(ret)
                                options.onResponse(ret);
                            }
                        },
                        fail: function (err) {
                            console.log('閿欒鐮侊細' + err.errCode);
                            console.log('閿欒淇℃伅锛�' + err.errMsg);
                        }
                    });
                });
            } else if (ua.indexOf('micromessenger') > -1) {
                loadScript("https://res.wx.qq.com/open/js/jweixin-1.3.2.js", function () {
                    wx.getLocation({
                        type: 'wgs84',
                        altitude: 'false',
                        success: function(res) {
                            if (options && options.onResponse) {
                                var ret = {
                                    code: "200",
                                    msg: "success",
                                    longitude: res.longitude,
                                    latitude: res.latitude
                                }
                                ret = JSON.stringify(ret)
                                options.onResponse(ret);
                            }
                        },
                        fail: function (err) {
                            console.log(JSON.stringify(err));
                        },
                        complete: function (com) {
                        }
                    })
                });
            } else if (isAlipayMp()) {
                loadScript("https://appx/web-view.min.js", function () {
                    my.getLocation({
                        success: function(res) {
                            if (options && options.onResponse) {
                                var ret = {
                                    code: "200",
                                    msg: "success",
                                    longitude: res.longitude,
                                    latitude: res.latitude
                                }
                                ret = JSON.stringify(ret)
                                options.onResponse(ret);
                            }
                        }
                    })
                });
            } else {
                window.WebViewJavascriptBridge.callHandler("getLocation", "", function(responseData) {
                    console.log("getLocation responseData:" + responseData);
                    if (options && options.onResponse) {
                        options.onResponse(responseData);
                    }
                });
            }
        },
        startShake:function(options) {
            window.WebViewJavascriptBridge.callHandler("startShake", "", function(responseData) {
                console.log("startShake responseData:" + responseData);
                if (options && options.onResponse) {
                    options.onResponse(responseData);
                }
            });
        },
        getBarcode:function(options) {
            window.WebViewJavascriptBridge.callHandler("getBarcode", "", function(responseData) {
                console.log("getBarcode responseData:" + responseData);
                if (options && options.onResponse) {
                    options.onResponse(responseData);
                }
            });
        },
        share:function(options) {
        	var ua = navigator.userAgent.toLowerCase();
            if (ua.indexOf('baidu') > -1) {
                loadScript("https://b.bdstatic.com/searchbox/icms/searchbox/js/swan-2.0.4.js", function () {
                    swan.openShare({
                        title: '澶╁簻甯傛皯浜�',
					    content: '涓€绔欏紡浜掕仈缃�+鍩庡競鏈嶅姟骞冲彴',
					    path: '/pages/index/index',
                        success: function (res) {
                            if (options && options.onResponse) {
                                options.onResponse(res);
                            }
                        },
                        fail: function (err) {
                            console.log('閿欒鐮侊細' + err.errCode);
                            console.log('閿欒淇℃伅锛�' + err.errMsg);
                        }
                    });
                });
            } if (isAlipayMp()) {
                loadScript("https://appx/web-view.min.js", function () {
                    my.alert({
                        title: '浜�',
                        content: '璇风偣鍑诲彸涓婅閫夐」杩涜鍒嗕韩鍝�',
                        buttonText: '鎴戠煡閬撲簡',
                        success: function() {
                        },
                    });
                });
            } else {
            	var string = '{"title":"' + options.title + '",' + '"content":"' + options.content + '",' + '"imgUrl":"' + options.imgUrl + '",' + '"link":"' + options.link + '",' + '"login":"' + options.login + '",' + '"platformType":' + JSON.stringify(options.platformType) + "}";
	            window.WebViewJavascriptBridge.callHandler("share", encodeURIComponent(string), function(responseData) {
	                console.log("share responseData:" + responseData);
	                if (options && options.onResponse) {
	                    options.onResponse(responseData);
	                }
	            });
            }
        },
        WXPay:function(options)聽{
            var聽string聽=聽'{"data":"'聽+聽options.data聽+聽'"}';
            window.WebViewJavascriptBridge.callHandler("WXPay",聽encodeURIComponent(string),聽function(responseData)聽{
                console.log("WXPay聽responseData:"聽+聽responseData);
                if聽(options聽&&聽options.onResponse)聽{
                    options.onResponse(responseData);
                }
            });
        },
        getUserInfo:function(options) {
            if (!isMp()) {
                window.WebViewJavascriptBridge.callHandler("getUserInfo", "", function(responseData) {
                    console.log("getBarcode responseData:" + responseData);
                    if (options && options.onResponse) {
                        options.onResponse(responseData);
                    }
                });
            }
        },
        realPersonAuth:function(options) {
            if (!isMp()) {
                var string = '{"clientId":"' + options.clientId + '",' + '"token":"' + options.token + '",' + '"authType":"' + options.authType + '"}';
                window.WebViewJavascriptBridge.callHandler("realPersonAuth", encodeURIComponent(string), function(responseData) {
                    if (options && options.onResponse) {
                        options.onResponse(responseData);
                    }
                });
            }
        },
        saveImage:function(options) {
            var string = '{"imgStr":"' + options.imgStr + '"}';
            window.WebViewJavascriptBridge.callHandler("saveImage", encodeURIComponent(string), function(responseData) {
                if (options && options.onResponse) {
                    options.onResponse(responseData);
                }
            });
        },
        getDeviceId:function(options) {
            if (!isMp()) {
                window.WebViewJavascriptBridge.callHandler("getDeviceId", "", function(responseData) {
                    console.log("getBarcode responseData:" + responseData);
                    if (options && options.onResponse) {
                        options.onResponse(responseData);
                    }
                });
            }
        },
        setScreenHighlight:function(options) {
            window.WebViewJavascriptBridge.callHandler("setScreenHighlight", "", function(responseData) {
                console.log("setScreenHighlight responseData:" + responseData);
                if (options && options.onResponse) {
                    options.onResponse(responseData);
                }
            });
        },
        openWebUrl:function(options) {
            var string = '{"webUrl":"' + options.webUrl + '",' + '"title":"' + options.title + '"}';
            window.WebViewJavascriptBridge.callHandler("openWebUrl", encodeURIComponent(string), function(responseData) {
                console.log("openWebUrl responseData:" + responseData);
                if (options && options.onResponse) {
                    options.onResponse(responseData);
                }
            });
        },
        resetScreenBrightness:function(options) {
            window.WebViewJavascriptBridge.callHandler("resetScreenBrightness", "", function(responseData) {
                console.log("resetScreenBrightness responseData:" + responseData);
                if (options && options.onResponse) {
                    options.onResponse(responseData);
                }
            });
        },
        setCityinfo:function(options) {
            var string = '{"cityCode":"' + options.cityCode + '",' + '"cityName":"' + options.cityName + '",' + '"cityNewsTitle":"' + options.cityNewsTitle + '",' + '"cityParent":"' + options.cityParent + '"}';
            window.WebViewJavascriptBridge.callHandler("setCityinfo", encodeURIComponent(string), function(responseData) {
                console.log("setCityinfo responseData:" + responseData);
                if (options && options.onResponse) {
                    options.onResponse(responseData);
                }
            });
        },
        getCityCode:function(options) {
            if(!isMp()) {
                window.WebViewJavascriptBridge.callHandler("getCityCode", "", function(responseData) {
                    console.log("getCityCode responseData:" + responseData);
                    if (options && options.onResponse) {
                        options.onResponse(responseData);
                    }
                });
            }
        },
	electronicOcr:function(options) {
            window.WebViewJavascriptBridge.callHandler("electronicOcr", "", function(responseData) {
                console.log("electronicOcr responseData:" + responseData);
                if (options && options.onResponse) {
                    options.onResponse(responseData);
                }
            });
        },
        /**
 *          * 鎵撳紑瀹炲悕璁よ瘉鏂瑰紡鍒楄〃
 *                   *
 *                            * @return code锛�200 鎴愬姛锛�201 鍙栨秷
 *                                     */
        openRealNameAuthenticationList:function(options) {
            window.WebViewJavascriptBridge.callHandler("openRealNameAuthenticationList", "", function(responseData) {
                console.log("openRealNameAuthenticationList responseData:" + responseData);
                if (options && options.onResponse) {
                    options.onResponse(responseData);
                }
            });
        },
        /**
 *          * 浜鸿劯璇嗗埆锛屽疄鍚嶈璇�
 *                   *
 *                            * @param token     鐢ㄦ埛淇℃伅鏍囪瘑
 *                                     * @param channelId 娓犻亾鍙�
 *                                              *
 *                                                       * @return code锛�200 鎴愬姛锛�201 鍙栨秷锛�500 閿欒
 *                                                                *         msg锛� 瀵� code 鐨勪竴鑸弿杩�
 *                                                                         */
        realNameAuthentication:function(options) {
            var string = '{"token":"' + (options.token ? options.token : '') + '",' +
                    '"channelId":"' + (options.channelId ? options.channelId : '') + '"}'
            window.WebViewJavascriptBridge.callHandler(
                "realNameAuthentication",
                encodeURIComponent(string),
                function(responseData) {
                    console.log("realNameAuthentication responseData:" + responseData);
                    if (options && options.onResponse) {
                        options.onResponse(responseData);
                    }
                }
            );
        },
        /**
 *          * 浜鸿劯璇嗗埆锛屽疄浜鸿璇侊紝浠呭湪鐢ㄦ埛宸插疄鍚嶆椂鎵嶆湁鐢�
 *                   *
 *                            * @param token
 *                                     * @param clientId
 *                                              *
 *                                                       * @return code锛�200 鎴愬姛锛�201 鍙栨秷锛�500 閿欒
 *                                                                *         msg锛� 瀵� code 鐨勪竴鑸弿杩�
 *                                                                         */
        facialIdentification:function(options) {
            var string = '{"token":"' + (options.token ? options.token : '') + '",' +
                    '"clientId":"' + (options.clientId ? options.clientId : '') + '"}'
            window.WebViewJavascriptBridge.callHandler(
                "facialIdentification",
                encodeURIComponent(string),
                function(responseData) {
                    console.log("facialIdentification responseData:" + responseData);
                    if (options && options.onResponse) {
                        options.onResponse(responseData);
                    }
                }
            );
        },
        showLoading:function(options) {
            window.WebViewJavascriptBridge.callHandler("showLoading", "", function(responseData) {
                console.log("showLoading responseData:" + responseData);
                if (options && options.onResponse) {
                    options.onResponse(responseData);
                }
            });
        },
        hideLoading:function(options) {
            window.WebViewJavascriptBridge.callHandler("hideLoading", "", function(responseData) {
                console.log("hideLoading responseData:" + responseData);
                if (options && options.onResponse) {
                    options.onResponse(responseData);
                }
            });
        },
	getScreenShotImage:function (options){
             window.WebViewJavascriptBridge.callHandler(
                "getScreenShotImage"
                ,""
                ,function (responseData) {
                    if(options && options.onResponse){
                         options.onResponse(responseData);
                    }
                }
           );
         },
	getEESBCardSign: function (options) {
            var string = '{\"domainStr\":\"'+options.domainStr+'\",'
                    +'\"signStr\":\"'+options.signStr+'\"}';
            window.WebViewJavascriptBridge.callHandler(
                'getEESBCardSign'
                ,encodeURIComponent(string)
                ,function (responseData) {
                    if(options && options.onResponse){
                        options.onResponse(responseData);
                    }
                }
            );
        },
	getPHAuthorization:function(options) {
            window.WebViewJavascriptBridge.callHandler("getPHAuthorization", "", function(responseData) {
                if (options && options.onResponse) {
                    options.onResponse(responseData);
                }
            });
        },
	liveAuthentication:function(options) {
            var string = '{"token":"' + (options.token ? options.token : '') + '",' + '"channelId":"' + (options.channelId ? options.channelId : '') + '"}'
            window.WebViewJavascriptBridge.callHandler(
                "liveAuthentication",
                encodeURIComponent(string),
                function(responseData) {
                    console.log("liveAuthentication responseData:" + responseData);
                    if (options && options.onResponse) {
                        options.onResponse(responseData);
                    }
                }
            );
        },
        //鏀胯瀺鏀粯(缁翠慨璧勯噾)
    CCBGovPay:function(options) {
            var string = '{"payTypeUrl":"' + options.payTypeUrl + '"}';
            window.WebViewJavascriptBridge.callHandler("CCBGovPay", encodeURIComponent(string), function(responseData) {
                console.log("CCBGovPay responseData:" + responseData);
                if (options && options.onResponse) {
                    options.onResponse(responseData);
                }
            });
        },
        //鑾峰彇鐗堟湰鍙�
        getAppVersion:function(options) {
            window.WebViewJavascriptBridge.callHandler("getAppVersion", "", function(responseData) {
                console.log("getAppVersion responseData:" + responseData);
                if (options && options.onResponse) {
                    options.onResponse(responseData);
                }
            });
        },
        //寮曞鐢ㄦ埛鐧诲綍
        userLogin:function(options) {
            window.WebViewJavascriptBridge.callHandler("userLogin", "", function(responseData) {
                console.log("userLogin responseData:" + responseData);
                if (options && options.onResponse) {
                    options.onResponse(responseData);
                }
            });
        },
        //寮曞鐢ㄦ埛瀹炲悕璁よ瘉
        userIdentityAuth:function(options) {
            window.WebViewJavascriptBridge.callHandler("userIdentityAuth", "", function(responseData) {
                console.log("userIdentityAuth responseData:" + responseData);
                if (options && options.onResponse) {
                    options.onResponse(responseData);
                }
            });
        },
        //鑾峰彇鐢ㄦ埛璐﹀彿鐘舵€�
        getUserStatus:function(options) {
            window.WebViewJavascriptBridge.callHandler("getUserStatus", "", function(responseData) {
                console.log("getUserStatus responseData:" + responseData);
                if (options && options.onResponse) {
                    options.onResponse(responseData);
                }
            });
        },

        //寮€濮嬬櫥褰�
    startLogin:function(options) {
            var string = '{"redirectUrl":"' + options.redirectUrl + '"}';
            window.WebViewJavascriptBridge.callHandler("startLogin", encodeURIComponent(string), function(responseData) {
                console.log("startLogin responseData:" + responseData);
                if (options && options.onResponse) {
                    options.onResponse(responseData);
                }
            });
        },
        //鑾峰彇鏀粯瀹濆拰寰俊鐨勬巿鏉僣ode,
        //鍏ュ弬:AuthCode涓�:WXAuth(寰俊)銆乤plipayAuth(鏀粯瀹�)
        //鍑哄弬:authCode(鎺堟潈鐮�)
        getBindingMobileAuthCode:function(options) {
            var string = '{"authType":"' + options.authType + '"}';
            window.WebViewJavascriptBridge.callHandler("getBindingMobileAuthCode", encodeURIComponent(string), function(responseData) {
                console.log("getBindingMobileAuthCode responseData:" + responseData);
                if (options && options.onResponse) {
                    options.onResponse(responseData);
                }
            });
        },
        //寮€濮嬭闊宠瘑鍒緭鍏ユ枃瀛�
        startRecord:function(options) {
            window.WebViewJavascriptBridge.callHandler("startRecord", "", function(responseData) {
                console.log("startRecord responseData:" + responseData);
                if (options && options.onResponse) {
                    options.onResponse(responseData);
                }
            });
        },
        //缁撴潫璇煶璇嗗埆骞惰繑鍥炶瘑鍒粨鏋�
        translateVoice:function(options) {
            window.WebViewJavascriptBridge.callHandler("translateVoice", "", function(responseData) {
                console.log("translateVoice responseData:" + responseData);
                if (options && options.onResponse) {
                    options.onResponse(responseData);
                }
            });
        },

        //鏅嘲淇濋櫓鏀粯
        ElectronicPay:function(options) {
            var string = '{"payInfo":"' + options.payInfo + '"}';
            window.WebViewJavascriptBridge.callHandler("ElectronicPay", encodeURIComponent(string), function(responseData) {
                console.log("ElectronicPay responseData:" + responseData);
                if (options && options.onResponse) {
                    options.onResponse(responseData);
                }
            });
        },
        //璺宠浆鍒板叾浠栧煄甯傞棬鎴�
        jumpToOtherCity:function(options) {
            var string = '{"cityCode":"' + options.cityCode + '",' + '"cityName":"' + options.cityName + '"}';
            window.WebViewJavascriptBridge.callHandler("jumpToOtherCity", encodeURIComponent(string), function(responseData) {
                console.log("jumpToOtherCity responseData:" + responseData);
                if (options && options.onResponse) {
                    options.onResponse(responseData);
                }
            });
        },

        //澶╁簻閫氭壂鐮佷箻杞﹁幏鍙栦綑棰�
        getTftBalance:function(options) {
            var string = '{"token":"' + options.token + '"}';
            window.WebViewJavascriptBridge.callHandler("getTftBalance", encodeURIComponent(string), function(responseData) {
                console.log("getTftBalance responseData:" + responseData);
                if (options && options.onResponse) {
                    options.onResponse(responseData);
                }
            });
        },

        //澶╁簻閫氭壂鐮佷箻杞﹀幓鍏呭€�
        tftRecharge:function(options) {
            var string = '{"token":"' + options.token + '"}';
            window.WebViewJavascriptBridge.callHandler("tftRecharge", encodeURIComponent(string), function(responseData) {
                console.log("tftRecharge responseData:" + responseData);
                if (options && options.onResponse) {
                    options.onResponse(responseData);
                }
            });
        },

        //鍩庡競婕父浣跨敤
    openRoamingService:function(options) {
            var string = '{"cityCode":"' + options.cityCode + '",' + '"serviceCode":"' + options.serviceCode + '"}'
            window.WebViewJavascriptBridge.callHandler(
                "openRoamingService", 
                encodeURIComponent(string), 
                function(responseData) {
                console.log("openRoamingService responseData:" + responseData);
                if (options && options.onResponse) {
                    options.onResponse(responseData);
                    }
                }
            );
        }
    }
    window.eshimin = new eshimin();
    if (window.WebViewJavascriptBridge) {
        return;
    }
    var messagingIframe;
    var sendMessageQueue = [];
    var receiveMessageQueue = [];
    var messageHandlers = {};
    var CUSTOM_PROTOCOL_SCHEME = "yy";
    var QUEUE_HAS_MESSAGE = "__QUEUE_MESSAGE__/";
    var responseCallbacks = {};
    var uniqueId = 1;
    function _createQueueReadyIframe(doc) {
        messagingIframe = doc.createElement("iframe");
        messagingIframe.style.display = "none";
        doc.documentElement.appendChild(messagingIframe);
    }
    function isAndroid() {
        var ua = navigator.userAgent.toLowerCase();
        var isA = ua.indexOf("android") > -1;
        if (isA) {
            return true;
        }
        return false;
    }
    function isIphone() {
        var ua = navigator.userAgent.toLowerCase();
        var isIph = ua.indexOf("iphone") > -1;
        if (isIph) {
            return true;
        }
        return false;
    }
    function isAlipayMp() {
        var ua = navigator.userAgent.toLowerCase();
        var isalipay = ua.indexOf("alipayclient") > -1 && ua.indexOf("miniprogram") > -1;
        if (isalipay) {
            return true;
        }
        return false;
    }
    function isMp() {
        var ua = navigator.userAgent.toLowerCase();
        var isTfsmy = ua.indexOf("tfsmy") > -1;
        if (!isTfsmy) {
            return true;
        }
        return false;
    }
    function init(messageHandler) {
        if (WebViewJavascriptBridge._messageHandler) {
            throw new Error("WebViewJavascriptBridge.init called twice");
        }
        WebViewJavascriptBridge._messageHandler = messageHandler;
        var receivedMessages = receiveMessageQueue;
        receiveMessageQueue = null;
        for (var i = 0; i < receivedMessages.length; i++) {
            _dispatchMessageFromNative(receivedMessages[i]);
        }
    }
    function send(data, responseCallback) {
        _doSend({
            data:data
        }, responseCallback);
    }
    function registerHandler(handlerName, handler) {
        messageHandlers[handlerName] = handler;
    }
    function callHandler(handlerName, data, responseCallback) {
        _doSend({
            handlerName:handlerName,
            data:data
        }, responseCallback);
    }
    function _doSend(message, responseCallback) {
        if (responseCallback) {
            var callbackId = "cb_" + uniqueId++ + "_" + new Date().getTime();
            responseCallbacks[callbackId] = responseCallback;
            message.callbackId = callbackId;
        }
        sendMessageQueue.push(message);
        messagingIframe.src = CUSTOM_PROTOCOL_SCHEME + "://" + QUEUE_HAS_MESSAGE;
    }
    function _fetchQueue() {
        var messageQueueString = JSON.stringify(sendMessageQueue);
        sendMessageQueue = [];
        if (isIphone()) {
            return messageQueueString;
        } else {
            if (isAndroid()) {
                messagingIframe.src = CUSTOM_PROTOCOL_SCHEME + "://return/_fetchQueue/" + messageQueueString;
            }
        }
    }
    function _dispatchMessageFromNative(messageJSON) {
        setTimeout(function() {
            var message = JSON.parse(messageJSON);
            var responseCallback;
            if (message.responseId) {
                responseCallback = responseCallbacks[message.responseId];
                if (!responseCallback) {
                    return;
                }
                responseCallback(message.responseData);
                delete responseCallbacks[message.responseId];
            } else {
                if (message.callbackId) {
                    var callbackResponseId = message.callbackId;
                    responseCallback = function(responseData) {
                        _doSend({
                            responseId:callbackResponseId,
                            responseData:responseData
                        });
                    };
                }
                var handler = WebViewJavascriptBridge._messageHandler;
                if (message.handlerName) {
                    handler = messageHandlers[message.handlerName];
                }
                try {
                    handler(message.data, responseCallback);
                } catch (exception) {
                    if (typeof console != "undefined") {
                        console.log("WebViewJavascriptBridge: WARNING: javascript handler threw.", message, exception);
                    }
                }
            }
        });
    }
    function _handleMessageFromNative(messageJSON) {
        if (receiveMessageQueue) {
            receiveMessageQueue.push(messageJSON);
        } else {
            _dispatchMessageFromNative(messageJSON);
        }
    }
    var WebViewJavascriptBridge = window.WebViewJavascriptBridge = {
        init:init,
        send:send,
        registerHandler:registerHandler,
        callHandler:callHandler,
        _fetchQueue:_fetchQueue,
        _handleMessageFromNative:_handleMessageFromNative
    };
    var doc = document;
    if(!isMp()) {
        _createQueueReadyIframe(doc);
    }
    var readyEvent = doc.createEvent("Events");
    readyEvent.initEvent("WebViewJavascriptBridgeReady");
    readyEvent.bridge = WebViewJavascriptBridge;
    doc.dispatchEvent(readyEvent);

    function loadScript(url, callback){
        if (typeof my !='undefined' || typeof wx !='undefined' || typeof swan !='undefined') {
            console.log('宸插姞杞借js');
        } else {
            var script = document.createElement("script");
            script.type = "text/javascript";
            if (script.readyState){ //IE
                script.onreadystatechange = function(){
                    if (script.readyState == "loaded" || script.readyState == "complete"){
                        script.onreadystatechange = null;
                        callback();
                    }
                };
            } else { //Others
                script.onload = function(){
                    callback();
                };
            }
            script.src = url;
            var heads = document.getElementsByTagName("head");
            if(heads.length) {
                heads[0].appendChild(script);
            } else {
                document.body.appendChild(script);
            }
        }
    }
})();